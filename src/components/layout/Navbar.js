import "../../assets/css/Navbar.css";
import logo from "../../assets/images/utilities/logo-color.png";
import { NavLink } from "react-router-dom";

const Navbar = () => {
	return (
		<nav className="Navbar">
			<div className="container">
				<div className="Navbar--flex">
					<div className="Navbar--item">
						<NavLink to={"/"}>
							<img src={logo} className="logo" alt="" />
						</NavLink>
					</div>
					<div className="Navbar--item">
						<div className="Navbar--links">
							<a href="#contact" className="btn btn-theme">Say Hello</a>
						</div>
					</div>
				</div>
			</div>
		</nav>
	)
}

export default Navbar;