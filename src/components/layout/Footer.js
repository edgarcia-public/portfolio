import "../../assets/css/rwd.css";
import { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";

const Footer = () => {
	const [input, setInput] = useState({ email: "", name: "" });
	const [error, setError] = useState("");
	const [isLoading, setIsLoading] = useState(false);

	const handleInput = (e) => {
		setInput({ ...input, [e.target.name]: e.target.value })
	}

	const handleSubmit = async (e) => {
		e.preventDefault();

		setIsLoading(true)

		if (!input.name && !input.email) {
			setError("All inputs are required.");
		} else {
			setError("Great! I'll get back to you ASAP.");
		}
	}

	useEffect(() => {
		setTimeout(() => {
			setError("");
			setInput({ email: "", name: "" })
			setIsLoading(false);
		}, 5000);
	}, [error]);

	return (
		<div className="Footer">
			<div className="separator"></div>
			<div className="container">
				<div className="Footer--grid">
					<div id="contact" className="Footer--grid-item" style={{ marginBottom: 30 }}>
						<h2 className="color-white">Let's connect!</h2>

						<form id="contactForm" onSubmit={handleSubmit}>
							<div className="input-wrapper">
								<input type="text" name="name" className="input-form" placeholder="Name" required value={input.name} onChange={e => handleInput(e)} />
							</div>
							<div className="input-wrapper">
								<input type="email" name="email" className="input-form" placeholder="Email Address" required value={input.email} onChange={e => handleInput(e)} />
							</div>
							{error &&
								<div className="notif">
									<img src={require("../../assets/images/utilities/exclamation.png")} alt="" />
									<p>{error}</p>
								</div>
							}
							<button type="submit" className="btn btn-theme" disabled={isLoading} onClick={handleSubmit}>Hit me up!</button>
						</form>
					</div>
					<div className="Footer--grid-item">
						<h2 className="color-white">Blogs <span style={{ color: "#aaa" }}>(soon!)</span></h2>
						<ul className="ul-list color-white">
							<li><NavLink to={"/"}>MERN stack journey</NavLink></li>
							<li><NavLink to={"/"}>Bootcamp projects</NavLink></li>
							<li><NavLink to={"/"}>My first JavaScript game</NavLink></li>
							<li><NavLink to={"/"}>OG basic portfolio</NavLink></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	)
}

export default Footer;