import { nanoid } from "nanoid";

export const funFactData = {
	html: [
		{
			id: nanoid(),
			title: "Building Blocks of the Web",
			image: "blocks.jpg",
			description: "HTML stands for HyperText Markup Language. It's the foundation of the World Wide Web and is used to structure content on websites. It uses elements like headings, paragraphs, links, and images to create the structure and layout of web pages."
		},
		{
			id: nanoid(),
			image: "tags.jpg",
			title: "Tags, Tags Everywhere",
			description: "HTML uses tags, which are enclosed in angle brackets, to define the structure and content of a webpage. Tags like <h1>, <p>, <a>, and <img> are examples of how HTML elements are marked up to display different types of content."
		},
		{
			id: nanoid(),
			title: "Nesting Like Russian Dolls",
			image: "dolls.jpg",
			description: "HTML elements can be nested inside each other. This nesting creates a hierarchy that defines the structure of a webpage. For example, a <div> element can contain multiple other elements like headings, paragraphs, and lists."
		},
		{
			id: nanoid(),
			title: "Evolution of HTML",
			image: "html.jpg",
			description: "HTML has gone through several versions, with HTML5 being the latest major version. HTML5 introduced new features like native video and audio support, canvas for drawing graphics, and semantic elements like <header>, <footer>, and <nav> to enhance the meaning and structure of web content."
		},
	],
	css: [
		{
			id: nanoid(),
			title: "Style Wizardry",
			image: "wizard.jpg",
			description: "CSS stands for Cascading Style Sheets. It's the magic behind the visual appeal of websites. CSS allows you to control the layout, colors, fonts, and other design aspects of web pages, turning plain HTML content into stunning visuals."
		},
		{
			id: nanoid(),
			title: "Selector Sorcery",
			image: "selector.jpg",
			description: "CSS uses selectors to target specific HTML elements for styling. You can use simple selectors like element names (div, p) or class and ID selectors (.classname, #idname) to apply styles to different parts of your webpage."
		},
		{
			id: nanoid(),
			title: "Cascade of Styles",
			image: "cascade.jpg",
			description: "The 'Cascading' in CSS refers to the order in which styles are applied. Styles can be defined inline within HTML, in a <style> block within the HTML document, or in an external CSS file. When multiple conflicting styles target the same element, the cascade decides which one takes precedence."
		},
		{
			id: nanoid(),
			title: "Responsive Reshaping",
			image: "responsive.jpg",
			description: "CSS empowers responsive web design. With media queries, you can create styles that adapt to different screen sizes, making your website look great on various devices. This ensures that your content is easily readable and usable, whether on a large desktop monitor or a small smartphone screen."
		},
	],
	bootstrap: [
		{
			id: nanoid(),
			title: "Rapid Styling Magic",
			image: "styling.jpg",
			description: "Bootstrap is a popular CSS framework developed by Twitter. It's like a pre-built toolkit that includes a variety of CSS styles, components, and JavaScript plugins. With Bootstrap, you can create sleek and responsive websites faster, as it provides a consistent foundation for design."
		},
		{
			id: nanoid(),
			title: "Grid Goodness",
			image: "grid.jpg",
			description: "One of Bootstrap's standout features is its responsive grid system. This 12-column grid makes it easy to create responsive layouts that adapt to different screen sizes. You can control how elements stack and rearrange on smaller screens, ensuring a smooth user experience across devices."
		},
		{
			id: nanoid(),
			title: "Component Treasure Trove",
			image: "component.jpg",
			description: "Bootstrap comes with a range of pre-designed components, such as navigation bars, buttons, forms, modals, and carousels. These components are already styled and can be customized to match your website's design. This saves you time and effort compared to building everything from scratch."
		},
		{
			id: nanoid(),
			title: "Theming Flexibility",
			image: "theme.jpg",
			description: "Bootstrap offers a theming system that allows you to customize the default styles to match your brand's look and feel. You can change colors, typography, and other design elements using variables and SASS (Syntactically Awesome Style Sheets), giving your website a unique appearance while still leveraging Bootstrap's benefits."
		},
	],
	javascript: [
		{
			id: nanoid(),
			title: "Dynamic Duo with HTML and CSS",
			image: "duo.jpg",
			description: "JavaScript is a versatile programming language that brings interactivity to websites. While HTML provides the structure and content, CSS handles the presentation, and JavaScript takes care of the behavior and interaction. Together, they form the foundation of modern web development."
		},
		{
			id: nanoid(),
			title: "Not Just for the Web",
			image: "event.jpg",
			description: "While JavaScript is primarily known for web development, it's not limited to browsers. With technologies like Node.js, JavaScript can be used on the server side to build applications, APIs, and more. This versatility makes it possible to use the same language for both client and server development."
		},
		{
			id: nanoid(),
			title: "Event-Driven Enchantment",
			image: "notjust.jpg",
			description: "JavaScript is event-driven, meaning it responds to user actions or events on a webpage. These events could be clicking a button, typing in a form, or even resizing the browser window. By attaching functions to events, you can create dynamic and interactive user experiences."
		},
		{
			id: nanoid(),
			title: "Eclectic Ecosystem",
			image: "eclectic.jpg",
			description: "JavaScript has a vast ecosystem of libraries and frameworks that make development even more efficient. Libraries like jQuery simplify common tasks, while frameworks like React, Angular, and Vue.js offer powerful tools for building complex user interfaces and applications."
		},
	],
	reactjs: [
		{
			id: nanoid(),
			title: "Component Playground",
			image: "playground.jpg",
			description: "React.js is a JavaScript library for building user interfaces. One of its core concepts is the use of components. A component is like a building block that encapsulates a piece of user interface and its behavior. These components can be reused, combined, and nested to create complex UIs."
		},
		{
			id: nanoid(),
			title: "Virtual DOM Wizardry",
			image: "virtual.jpg",
			description: "React introduced the concept of the Virtual DOM. Instead of directly manipulating the actual DOM (Document Object Model), React creates a lightweight virtual representation of it. When changes are made, React efficiently updates the virtual DOM and then computes the most efficient way to update the actual DOM, reducing unnecessary reflows and repaints."
		},
		{
			id: nanoid(),
			title: "Unidirectional Data Flow",
			image: "unidirectional.jpg",
			description: "React follows a unidirectional data flow, also known as the one-way data binding. Data flows in a single direction—from parent components to child components. This approach simplifies debugging, as changes in data can be traced more easily through the component hierarchy."
		},
		{
			id: nanoid(),
			title: "React Native Mobile",
			image: "native.jpg",
			description: "React.js' influence extends beyond the web. React Native, an extension of React, enables you to build mobile applications using the same concepts and codebase as React web applications. This means you can use your existing skills to create native iOS and Android apps."
		},
	],
	expressjs: [
		{
			id: nanoid(),
			title: "Minimalist Marvel",
			image: "marvel.jpg",
			description: "Express.js is a minimal and flexible web application framework for Node.js. It provides a set of robust features for building web and mobile applications. It's not a full-stack framework like some others, but rather focuses on providing the tools needed for creating server-side applications."
		},
		{
			id: nanoid(),
			title: "Routing Awesome",
			image: "routing.jpg",
			description: "One of the standout features of Express.js is its routing system. With Express, you can define routes to different URL paths and specify what should happen when a user accesses that path. This makes it easy to handle different parts of your application and respond with appropriate content or actions"
		},
		{
			id: nanoid(),
			title: "Middleware Magic",
			image: "eclectic.jpg",
			description: "Middleware functions in Express.js allow you to add functionality to your application's request-response cycle. You can use middleware to perform tasks such as authentication, logging, handling errors, and more. This modular approach makes your codebase more organized and easier to maintain."
		},
		{
			id: nanoid(),
			title: "Vibrant Ecosystem",
			image: "ecosystem.jpg",
			description: "While Express.js is minimal, its ecosystem is rich with third-party middleware and plugins that you can use to extend its capabilities. Whether you need authentication, database integration, or API handling, chances are there's a well-maintained middleware available to save you time and effort."
		},
	],
	mongodb: [
		{
			id: nanoid(),
			title: "Document Database Delight",
			image: "database.jpg",
			description: "MongoDB is a popular NoSQL database system known for its document-oriented approach. Unlike traditional relational databases, MongoDB stores data in flexible, JSON-like documents. This structure allows for dynamic and schema-less data modeling, making it ideal for applications with evolving data requirements."
		},
		{
			id: nanoid(),
			title: "Scaling Simplicity",
			image: "scaling.jpg",
			description: "MongoDB excels at horizontal scaling. It uses a technique called sharding, where data is distributed across multiple servers or clusters. This helps handle high traffic and large datasets, allowing your application to grow seamlessly as demand increases."
		},
		{
			id: nanoid(),
			title: "Query Language Flexibility",
			image: "query.jpg",
			description: "MongoDB's query language supports a wide range of queries, including geospatial queries, text searches, and aggregation pipelines. It also includes powerful features like indexing and aggregation stages that allow you to manipulate and transform data within the database itself."
		},
		{
			id: nanoid(),
			title: "Built-in High Availability",
			image: "availability.jpg",
			description: "MongoDB provides built-in features for high availability and fault tolerance. Replica sets allow you to maintain multiple copies of your data across different servers, ensuring that your application remains available even if one server goes down. This is crucial for mission-critical applications."
		}
	],
	nodejs: [
		{
			id: nanoid(),
			title: "V8 Engine",
			image: "engine.jpg",
			description: "Node.js is built on the V8 JavaScript engine developed by Google, which provides high-performance and efficient execution of JavaScript code."
		},
		{
			id: nanoid(),
			title: "Non-blocking I/O",
			image: "event.jpg",
			description: "Node.js uses an event-driven, non-blocking I/O model, making it suitable for handling a large number of concurrent connections and requests."
		},
		{
			id: nanoid(),
			title: "Package Manager",
			image: "notjust.jpg",
			description: "Node.js comes with npm (Node Package Manager), one of the largest ecosystems of open-source libraries and tools, making it easy to share and reuse code."
		},
		{
			id: nanoid(),
			title: "Full Stack JavaScript",
			image: "marvel.jpg",
			description: "With Node.js, you can build full-stack applications entirely in JavaScript, from the server-side to the client-side, streamlining development."
		}
	],
	npm: [
		{
			id: nanoid(),
			title: "Massive Package Collection",
			image: "notjust.jpg",
			description: "NPM hosts over a million packages, providing a vast range of ready-to-use libraries, tools, and frameworks that developers can easily incorporate into their projects."
		},
		{
			id: nanoid(),
			title: "Semantic Versioning",
			image: "versioning.jpg",
			description: "NPM uses semantic versioning, or SemVer, to ensure consistency and predictability when managing package dependencies, helping developers prevent compatibility issues."
		},
		{
			id: nanoid(),
			title: "Scripts and Automation",
			image: "automation.jpg",
			description: "NPM allows developers to define custom scripts in the package.json file, enabling automated tasks such as testing, building, and deployment with simple commands."
		},
		{
			id: nanoid(),
			title: "Global and Local Packages",
			image: "global.jpg",
			description: "NPM supports the installation of packages globally for system-wide usage or locally within specific projects, providing flexibility in managing dependencies."
		}
	],
	mongoose: [
		{
			id: nanoid(),
			title: "ODM for MongoDB",
			image: "database.jpg",
			description: "Mongoose.js is an Object Data Modeling (ODM) library for MongoDB, allowing you to define data structures and relationships in a more intuitive and organized way."
		},
		{
			id: nanoid(),
			title: "Schema and Validation",
			image: "schema.jpg",
			description: "With Mongoose, you can create schemas that define the structure of your data, as well as apply validation rules to ensure the integrity of your MongoDB documents."
		},
		{
			id: nanoid(),
			title: "Middleware Support",
			image: "notjust.jpg",
			description: "Mongoose provides middleware hooks that allow you to define functions to run before or after certain actions, making it easy to implement custom logic and behavior."
		},
		{
			id: nanoid(),
			title: "Querying and Population",
			image: "query.jpg",
			description: "Mongoose offers a powerful querying API with features like filtering, sorting, and pagination. Additionally, it supports population to retrieve related data from other collections."
		}
	],
	router: [
		{
			id: nanoid(),
			title: "Declarative Routing",
			image: "routing.jpg",
			description: "React Router allows you to declare routes using a declarative approach, making it easier to define the navigation structure of your React applications."
		},
		{
			id: nanoid(),
			title: "Nested Routing",
			image: "unidirectional.jpg",
			description: "With React Router, you can create nested route structures, enabling you to build complex UI layouts and hierarchies without compromising on navigation logic."
		},
		{
			id: nanoid(),
			title: "History Management",
			image: "tags.jpg",
			description: "React Router handles history management for single-page applications, providing features like browser history manipulation and programmatic navigation."
		},
		{
			id: nanoid(),
			title: "Dynamic Routing",
			image: "selector.jpg",
			description: "Dynamic routing in React Router allows you to generate routes based on data, enabling dynamic content rendering and efficient handling of varying data sets."
		}
	],
	zustand: [
		{
			id: nanoid(),
			title: "Lightweight",
			image: "versioning.jpg",
			description: "Zustand is a lightweight state management library for React applications, which means it won't bloat your bundle size."
		},
		{
			id: nanoid(),
			title: "Easy to Use",
			image: "selector.jpg",
			description: "Zustand is known for its simplicity and ease of use. Developers can quickly grasp its concepts and start using it."
		},
		{
			id: nanoid(),
			title: "No Boilerplate",
			image: "engine.jpg",
			description: "One of the great things about Zustand is that it requires minimal boilerplate code, making your codebase cleaner and more maintainable."
		},
		{
			id: nanoid(),
			title: "Optimized for Performance",
			image: "scaling.jpg",
			description: "Zustand is optimized for performance, ensuring your React applications run smoothly even with complex state management logic."
		}
	],
	git: [
		{
			id: nanoid(),
			title: "Distributed Version Control",
			image: "versioning.jpg",
			description: "Git is a distributed version control system, which means that every developer working on a project has their own copy of the entire repository, enabling offline work and efficient collaboration."
		},
		{
			id: nanoid(),
			title: "Fast and Efficient",
			image: "fast.jpg",
			description: "Git is designed to be fast and efficient, allowing for quick commits, branching, and merging. Its performance is essential for large codebases and complex projects."
		},
		{
			id: nanoid(),
			title: "Branching and Merging",
			image: "branch.jpg",
			description: "Git's branching and merging capabilities empower developers to work on different features or fixes simultaneously and merge changes back together, maintaining a clean codebase."
		},
		{
			id: nanoid(),
			title: "Commit History Visualization",
			image: "database.jpg",
			description: "Git provides a visual representation of the commit history, making it easier to track changes, understand development timelines, and troubleshoot issues."
		}
	],
	trello: [
		{
			id: nanoid(),
			title: "Visual Kanban Boards",
			image: "wizard.jpg",
			description: "Trello is known for its intuitive Kanban-style boards that visually organize tasks, making it easy to track progress, assign responsibilities, and manage projects."
		},
		{
			id: nanoid(),
			title: "Drag-and-Drop Interface",
			image: "wizard.jpg",
			description: "Trello's drag-and-drop interface allows users to effortlessly move cards between lists and boards, providing a user-friendly way to manage tasks and workflows."
		},
		{
			id: nanoid(),
			title: "Customizable Workflows",
			image: "wizard.jpg",
			description: "Trello lets you create custom workflows with lists, labels, and due dates, tailoring the platform to suit your team's specific project management needs."
		},
		{
			id: nanoid(),
			title: "Integration Capabilities",
			image: "wizard.jpg",
			description: "Trello supports integrations with various apps and tools, allowing you to connect your boards to other services for streamlined communication and enhanced productivity."
		}
	]
}

export const techDesc = {
	html: "HTML, Hypertext Markup Language, structures web content. It uses tags to define elements like headings, paragraphs, images, and links. Essential for web development, HTML forms the foundation of every webpage, enabling proper organization and presentation of information.",
	css: "CSS, Cascading Style Sheets, adds design to HTML content. It controls layout, colors, fonts, and responsiveness, enhancing web appearance. Essential for frontend development, CSS ensures consistent and visually appealing designs across various devices, contributing to a seamless user experience.",
	bootstrap: "Bootstrap, a CSS framework, expedites web development. It provides ready-made components and responsive grids, aiding in creating modern and adaptive websites. Widely used by developers, Bootstrap streamlines frontend design, fostering efficient and consistent UI development across different platforms.",
	javascript: "JavaScript, a versatile scripting language, powers web interactivity. Employed in frontend and backend development, it enables dynamic content, animations, and user-driven actions on websites. Widely adopted, JavaScript is integral to modern web applications, enhancing user engagement and functionality.",
	reactjs: "ReactJS, a JavaScript library, simplifies UI creation. It builds dynamic user interfaces using reusable components, enhancing web app performance. Employed in frontend development, ReactJS enables efficient updates without full page reloads, resulting in seamless and responsive user experiences.",
	expressjs: "ExpressJS, a minimal Node.js framework, streamlines backend development. It simplifies building robust APIs and web applications. With middleware support, routing, and modular structure, ExpressJS enables efficient server-side coding. It's favored by developers for creating scalable and performant backend systems.",
	mongodb: "MongoDB, a NoSQL database, stores data in flexible JSON-like documents. It's designed for scalability and performance, crucial for modern applications. Its schema-less structure accommodates evolving data needs, making it ideal for agile development. MongoDB's querying and indexing capabilities support efficient data retrieval, aligning with your role as a developer focused on solving real-world problems and building user-friendly apps.",
	nodejs: "Node.js is a runtime environment that allows executing JavaScript code on the server side. It enables scalable and event-driven applications, aligning with your role in web development.",
	npm: "npm is the Node Package Manager, facilitating the installation and management of packages and dependencies for your projects. It aids in efficient code development and deployment.",
	mongoose: "Mongoose is an ODM library for MongoDB and Node.js. It simplifies database interactions, providing a schema-based approach to model and manage data in MongoDB.",
	router: "A router directs traffic within web applications. It manages URLs and route handling, ensuring smooth navigation. As a developer, routers help organize and structure your projects.",
	zustand: "Zustand is a React state management library that's lightweight, user-friendly, boilerplate-free, and finely tuned for high-performance apps.",
	git: "Git is a version control system that tracks code changes collaboratively. It enables branching, merging, and history tracking, fostering efficient teamwork and code management.",
	trello: "Trello is a project management tool based on cards and boards. It aids in task organization, collaboration, and progress tracking, facilitating streamlined project management in your role."
}
