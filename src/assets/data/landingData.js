import { nanoid } from "nanoid";

export const techJourneyData = [
	{
		id: 1,
		date: "Jan 2018 - Present",
		title: "Digital Marketing",
		org: "Alpha Lab IT Consulting | W. O'Donnell Consulting",
		story: "For the past five years, I've been a valuable member of a dynamic digital marketing agency located in the heart of New York City. Throughout my time there, I've had the opportunity to hone my skills in a variety of areas critical to our industry, including search engine ranking, WordPress development and deployment, and copywriting. Thanks to the stimulating and fast-paced environment at the agency, I've been able to stay at the cutting edge of the latest developments in digital marketing, and I'm eager to leverage this experience in new and exciting roles moving forward.",
		ref: { name: "W. O'Donnell Consulting", url: "https://marketing.wodonnell.com/about/our-team/" }
	},
	{
		id: 2,
		date: "Nov 2022 - May 2023",
		title: "MERN Stack Bootcamp",
		org: "Uplift Code Camp | MERN Full Stack",
		story: "As a proud Aboitiz scholar, I was privileged to participate in Uplift Code Camp's intensive MERN Stack Bootcamp. Throughout the program, I received comprehensive training in a wide range of cutting-edge technologies, from HTML to Data Structures to ReactJS, and beyond. I am grateful to Aboitiz for their tremendous generosity in providing this opportunity, and to Uplift for their unwavering commitment to helping aspiring tech professionals like myself upskill and excel in the industry.",
		ref: { name: "Uplift Code Camp", url: "https://www.upliftcodecamp.com/" }
	},
	{
		id: 3,
		date: "2015",
		title: "Website Development",
		org: "TESDA | Website Development (LAMP Full Stack)",
		story: "My journey in software development began in 2016, when TESDA unveiled a comprehensive curriculum for Website Development. As part of the program, I delved into a variety of essential technologies, from HTML and CSS to basic JavaScript, PHP, and MySQL. Thanks to this rigorous training, I developed a deep passion for software development, and I've been eager to continue learning and growing ever since.",
		ref: { name: "TESDA", url: "https://car.tesda.gov.ph/tesdacar/list-of-tesda-courses/" }
	},
	{
		id: 4,
		date: "2005 - 2016",
		title: "IT Engineer",
		org: "BPO Industry | IT Support Engineer",
		story: "The BPO industry has been a critical part of my journey in technology. Thanks to the dynamic and ever-evolving nature of this sector, I've had the opportunity to work across a wide range of tech fields, from networking and administration to IT support, web servers, and beyond. Throughout my time in the industry, I've been driven by a relentless curiosity and a passion for staying at the forefront of the latest trends and developments in tech.",
		ref: { name: "", url: "" }
	},
]

export const projectData = [
	{
		id: 7,
		title: "ToBuy",
		link: "https://edmgarcia-tobuy.netlify.app/",
		label: "Try it out",
		type: "Utility",
		desc: "The only ToBuy list you'll ever need.",
		technologies: ["HTML", "CSS", "RWD", "MERN", "Full Stack"],
		github: "https://gitlab.com/poi-projects/reactjs-projects/buyme"
	},
	{
		id: 6,
		title: "RegBot",
		link: "https://edmgarcia-regbot.netlify.app/",
		label: "Let's rock!",
		type: "Utility",
		desc: "A clever way for app registration.",
		technologies: ["HTML", "CSS", "RWD", "ReactJS"],
		github: "https://gitlab.com/poi-projects/reactjs-projects/chatreg"
	},
	{
		id: 5,
		title: "PokeDex",
		link: "https://edmgarcia-pokedex.netlify.app/",
		label: "I choose you!",
		type: "Entertainment",
		desc: "An online Pokemon dictionary.",
		technologies: ["HTML", "CSS", "RWD", "ReactJS", "React Router"],
		github: "https://gitlab.com/poi-projects/reactjs-projects/pokemon"
	},
	{
		id: 4,
		title: "Unzooom",
		link: "https://edmgarcia.github.io/unzooom/",
		label: "Let's go!",
		type: "Game",
		desc: "Are you up for an eye challenge?",
		technologies: ["HTML", "CSS", "RWD", "JavaScript"],
		github: "https://github.com/edmgarcia/edmgarcia.github.io/tree/main/unzooom"
	},
	{
		id: 3,
		title: "DigiClock",
		link: "https://edmgarcia.github.io/digiclock/",
		label: "Try it out",
		type: "Utility",
		desc: "A minimalistic digital clock.",
		technologies: ["HTML", "CSS", "RWD", "JavaScript"],
		github: "https://github.com/edmgarcia/edmgarcia.github.io/tree/main/digiclock"
	},
	{
		id: 2,
		title: "Hide Name",
		link: "https://edmgarcia-hide-name.netlify.app/",
		label: "Try it out",
		type: "Utility",
		desc: "Make your name private the GCash way.",
		technologies: ["HTML", "CSS", "RWD", "JavaScript"],
		github: "https://gitlab.com/poi-projects/reactjs-projects/hide-name"
	},
	{
		id: 1,
		title: "Triangle Loop",
		link: "https://edmgarcia-triangle-loop.netlify.app/",
		label: "Let's chat!",
		type: "Game",
		desc: "Find your favorite triangle shape.",
		technologies: ["HTML", "CSS", "RWD", "JavaScript"],
		github: "https://gitlab.com/poi-projects/reactjs-projects/triangle-loop"
	},
]

export const projectGrid = [
	{
		id: nanoid(),
		layout: "odd",
		category: "utility",
		title: "ToBuy -- a hybrid shopping list",
		description: "Introducing a full-stack ToBuy app that enables you to create a list of items you're planning to buy. You can add a description for each item on your list, ensuring that you note down important information about the items. Additionally, you can include the store name for each list, and the app will utilize Google Maps to help you locate the nearest branch.",
		image: {
			file: "tobuy.png",
			class: ""
		},
		cta: {
			label: "Try it out",
			link: "https://edmgarcia-tobuy.netlify.app/"
		},
		stack: ["HTML", "CSS", "RWD", "ReactJS", "React Router", "Express", "Axios", "MongoDB", "Mongoose", "JWT", "BCrypt", "RESTful API", "NodeJS"],
		repo: {
			label: "Repository ↗",
			link: "https://gitlab.com/poi-projects/reactjs-projects/buyme"
		}
	},
	{
		id: nanoid(),
		layout: "even",
		category: "entertainment",
		title: "Pokémon Directory -- Pikachu, I choose you!",
		description: "Introducing the Poké Directory, a web app designed exclusively for Pokémon enthusiasts. Explore a meticulously organized collection, categorizing each unique Pokémon species and showcasing them in the style of authentic trading cards.",
		image: {
			file: "pokedex.png",
			class: ""
		},
		cta: {
			label: "I choose you!",
			link: "https://edmgarcia-pokedex.netlify.app"
		},
		stack: ["HTML", "CSS", "RWD", "ReactJS", "React Router", "Axios", "NodeJS"],
		repo: {
			label: "Repository ↗",
			link: "https://gitlab.com/poi-projects/reactjs-projects/pokemon"
		}
	},
	{
		id: nanoid(),
		layout: "odd",
		category: "game",
		title: "Unzooom! -- are you ready for an optical challenge?",
		description: "Put your focus and imagination to the test with a nostalgic nod to 90's gaming. Have fun!",
		image: {
			file: "unzooom.png",
			class: ""
		},
		cta: {
			label: "Let's rock!",
			link: "https://edmgarcia.github.io/unzooom/"
		},
		stack: ["HTML", "CSS", "RWD", "JavaScript"],
		repo: {
			label: "Repository ↗",
			link: "https://github.com/edmgarcia/edmgarcia.github.io/tree/main/unzooom"
		}
	},
	{
		id: nanoid(),
		layout: "even",
		category: "utility",
		title: "DigiClock -- your clock, your style",
		description: "An adaptable digital clock tailored to your timezone. Personalize its appearance by selecting colors and font styles that resonate with you. Moreover, experience an automated background transition that corresponds to morning, afternoon, evening, and dawn.",
		image: {
			file: "digiclock.png",
			class: "digiclock-image"
		},
		cta: {
			label: "Try it out",
			link: "https://edmgarcia.github.io/digiclock/"
		},
		stack: ["HTML", "CSS", "RWD", "JavaScript"],
		repo: {
			label: "Repository ↗",
			link: "https://github.com/edmgarcia/edmgarcia.github.io/tree/main/digiclock"
		}
	},
	{
		id: nanoid(),
		layout: "odd",
		category: "tool",
		title: "Chatbot Registration -- a modern way sign up process",
		description: "Crafted using vanilla JavaScript and designed with a mobile-ready approach, the Chatbot Registration offers an innovative and captivating method for your users to seamlessly sign up for your app with an AI-like feel.",
		image: {
			file: "chatreg.png",
			class: "chatreg-image"
		},
		cta: {
			label: "Let's chat!",
			link: "https://edmgarcia-regbot.netlify.app/"
		},
		stack: ["HTML", "CSS", "RWD", "JavaScript"],
		repo: {
			label: "Repository ↗",
			link: "https://gitlab.com/poi-projects/reactjs-projects/chatreg"
		}
	},
]