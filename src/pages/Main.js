import Navbar from "../components/layout/Navbar";
import Footer from "../components/layout/Footer";
import { Outlet } from "react-router-dom";

const Main = () => {
	return (
		<div className="Main">
			<Navbar />

			<div className="main--outlet">
				<Outlet />
			</div>

			<Footer />
		</div>
	)
}

export default Main;