const GridProjectItem = ({ data }) => {
	return (
		<div className={`project-grid-item ${data.layout}`}>
			<div className={`project-grid-child ${data.layout}-left`}>
				<div className="project-info">
					<h4 className="project-subtitle">{data.category}</h4>
					<h3 className="project-title">{data.title}</h3>
					<p className="project-description">{data.description}</p>

					<a href={data["cta"].link} className="btn-link-cta" target="_new">{data.cta.label}</a>

					<div className="project-tech-stack">
						<h5 style={{ marginBottom: 15 }} className="project-subtitle">TECH STACK</h5>
						<ul className="capsule-list">
							{
								data["stack"].map((stack, index) => <li key={index} className="capsule--item">{stack}</li>)
							}
							<li className="capsule--item">
								<a href={data["repo"].link} target="_new">{data["repo"].label}</a>
							</li>
						</ul>
					</div>
				</div>
			</div>

			<div className={`project-grid-child ${data.layout}-right`}>
				<img src={require(`../../../assets/images/utilities/${data["image"].file}`)} className={`project-image ${data["image"].class}`} alt="" />
			</div>
		</div>
	)
}

export default GridProjectItem;