import "../../../assets/css/GridProjects.css";
import { projectGrid } from "../../../assets/data/landingData";
import GridProjectItem from "./GridProjectItem";

const GridProjects = () => {
	return (
		<section className="Grid-projects section-panel">
			<div className="container">
				<h1 className="section-title text-center">Projects</h1>

				<div className="project-grid-parent">
					{Object.keys(projectGrid).map(key => <GridProjectItem key={projectGrid[key].id} data={projectGrid[key]} />)}
				</div>
			</div>
		</section>
	)
}

export default GridProjects;