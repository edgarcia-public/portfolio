import ProjectItem from "./ProjectItem";
import { projectData } from "../../../assets/data/landingData";

const Projects = () => {
	return (
		<section className="Projects section-panel">
			<div className="container">
				<h1 className="section-title text-center">Bootcamp & personal projects</h1>

				<div className="Project--list">
					{projectData.map((item, index) => <ProjectItem key={index} projects={item} />)}
				</div>
			</div>
		</section>
	)
}

export default Projects;