const ProjectItem = ({ projects }) => {
	return (
		<div className="Project--item">
			<div className="Project--item-wrapper">
				<div className="Project--item-head">
					<a href={projects.link} className="Project--title text-link" target="_new">{projects.title}</a> <span className="type">{projects.type}</span>
				</div>

				<div className="Project--item-body">
					<p className="Project--desc">{projects.desc}</p>
				</div>

				<div className="Project--item-footer">
					<p className="text-label">Technologies:</p>
					<ul className="capsule-list">
						{projects.technologies.map((item, index) => <li key={index} className="capsule--item">{item}</li>)}

						{projects.github ?
							<li className="capsule--item">
								<a href={projects.github} target="_new" >Repository ↗</a>
							</li>
							:
							undefined
						}
					</ul>
				</div>
			</div>
		</div>
	)
}

export default ProjectItem;