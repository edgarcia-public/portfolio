import Welcome from "./Welcome";
import Hello from "./Hello";
import Journey from "./journey/Journey";
import Technologies from "./Technologies";
import GridProjects from "./projects/GridProjects";

const Landing = () => {

	window.addEventListener("scroll", () => {
		const journeyTimeline = document.getElementById("journeyTimeline");
		const timelineZero = document.getElementById("timeline-0");
		const timelineOne = document.getElementById("timeline-1");
		const timelineTwo = document.getElementById("timeline-2");
		const timelineThree = document.getElementById("timeline-3");

		if (journeyTimeline) {
			const timelineZeroPosition = timelineZero.getBoundingClientRect().top;
			const timelineOnePosition = timelineOne.getBoundingClientRect().top;
			const timelineTwoPosition = timelineTwo.getBoundingClientRect().top;
			const timelineThreePosition = timelineThree.getBoundingClientRect().top;

			if (timelineZeroPosition <= 330) {
				timelineZero.classList.add("active");
			} else {
				timelineZero.classList.remove("active");
			}

			if (timelineOnePosition <= 480) {
				timelineOne.classList.add("active");
			} else {
				timelineOne.classList.remove("active");
			}

			if (timelineTwoPosition <= 580) {
				timelineTwo.classList.add("active");
			} else {
				timelineTwo.classList.remove("active");
			}

			if (timelineThreePosition <= 680) {
				timelineThree.classList.add("active");
			} else {
				timelineThree.classList.remove("active");
			}
		}

	});

	document.title = "Howdy, I'm Ed Garcia!"
	return (
		<section className="Landing">
			<Welcome />
			<Hello />
			<Journey />
			<GridProjects />
			<Technologies />
		</section>
	)
}

export default Landing;