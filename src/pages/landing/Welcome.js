import heroImage from "../../assets/images/utilities/me.png";
import tadaImage from "../../assets/images/utilities/tada.png";
import "../../assets/css/Landing.css";

const Welcome = () => {
	return (
		<section className="Welcome">
			<div className="container">
				<div className="Welcome--centered">
					<div className="Welcome--hero">
						<h1 className="hero--heading">
							<span className="color-theme">M</span>ongoDB + <span className="color-theme">E</span>xpress + <span className="color-theme">R</span>eact + <span className="color-theme">N</span>ode
						</h1>
						<p>are some of the programming technologies I use for building beautiful & dynamic web apps.</p>
						<div className="hero--image"
							style={{ backgroundImage: `url(${heroImage})` }}
							onClick={e => e.target.style.backgroundImage = `url(${tadaImage})`}
							onMouseOut={e => e.target.style.backgroundImage = `url(${heroImage})`}
						></div>
						<small className="color-gray">don't click me ;)</small>
					</div>
				</div>
			</div>
		</section>
	)
}

export default Welcome;