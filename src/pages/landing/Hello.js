import { useState } from "react";
import Confetti from "react-confetti";
import arrow from "../../assets/images/utilities/arrow.gif";

const Hello = () => {
	const [isTrue, setIsTrue] = useState(false);

	return (
		<section className="Hello section-panel">
			<div className="container">
				<div className="Hello--content">
					{
						isTrue &&
						<Confetti
							className="confetti"
							recycle={false}
							gravity={0.05}
							numberOfPieces={300}
							tweenDuration={5000}
							onConfettiComplete={() => setIsTrue(false)}
						/>
					}
					<div className="Hello--content-left color-white">
						<h2>Hello, I'm Ed! {isTrue ? <>👏🎉🥳</> : <>👋</>}</h2>
						<h3>I'm excited to work with you!</h3>
						<p>My 15 years of diverse technology experience began with my first text message on a Nokia 3310 -- it was in 2004. Since then, my passion for technology has been unwavering.</p>

						<p>I have honed my skills in networking, administration, web servers, and other digital services. However, software development is where my true passion lies. I thoroughly enjoy building web apps from scratch, which involves navigating through all stages of the software lifecycle. This includes establishing user stories, gathering resources, and execution. Throughout my tenure, I have both project-managed and built software, successfully delivering results.</p>

						<p>I am confident that my experiences in different technology fields plus my problem solving skills and attention to detail, combined with enthusiasm will make me a valuable addition to any development team.</p>
					</div>
					<div className="Hello--content-right">
						<div className={isTrue ? "Hello-cellphone-text text-center" : "Hello-cellphone-text"}>
							{isTrue ? "Message Sent ✉" : "Hey, Donna! Watya doin'? ♥"}
						</div>
						{isTrue ? "" : <img src={arrow} className="arrow" alt="" />}
						<button className="Hello-cellphone-btn btn" onClick={e => setIsTrue(!isTrue)}>{isTrue ? "Stop" : "Send"}</button>
					</div>
				</div>
			</div>
		</section>
	)
}

export default Hello;