import { useEffect, useRef, useState } from "react";
import { SiHtml5, SiCss3, SiBootstrap, SiJavascript, SiReact, SiExpress, SiMongodb, SiMongoose, SiNodedotjs, SiReactrouter, SiNpm, SiGit } from "react-icons/si";
import { RiBearSmileFill } from "react-icons/ri";
import { GrClose } from "react-icons/gr";
import { BsChevronRight } from "react-icons/bs";
import { funFactData, techDesc } from "../../assets/data/funFactData";
import "../../assets/css/funFact.css";
import Masonry from "masonry-layout";

const Technologies = () => {
	const [icon, setIcon] = useState(false);
	const gridRef = useRef(null);
	const techIcons = {
		HTML: <SiHtml5 />,
		CSS: <SiCss3 />,
		Bootstrap: <SiBootstrap />,
		JavaScript: <SiJavascript />,
		ReactJS: <SiReact />,
		ExpressJS: <SiExpress />,
		MongoDB: <SiMongodb />,
		NodeJS: <SiNodedotjs />,
		NPM: <SiNpm />,
		Mongoose: <SiMongoose />,
		Router: <SiReactrouter />,
		Zustand: <RiBearSmileFill />,
		Git: <SiGit />,
	}

	useEffect(() => {
		const masonry = new Masonry(gridRef.current, {
			itemSelector: ".fun-fact-panel",
			columnWidth: ".fun-fact-sizer",
			gutter: 30,
			percentPosition: true,
		});

		masonry.layout();
	}, [icon]);

	const handleClick = (value) => {
		const scrollUpHere = document.getElementById("techStackTitle");
		setIcon(value);
		setTimeout(() => {
			scrollUpHere.scrollIntoView({ behavior: "smooth" });
		}, 100);
	}

	const handleNext = () => {
		const scrollUpHere = document.getElementById("techStackTitle");
		const techStackContainer = document.getElementById("techStackContainer");
		const iconIndexCount = Object.keys(techIcons).length
		const currentIcon = icon.toLowerCase();
		const techIconsArr = Object.entries(techIcons)
		const iconIndex = techIconsArr.findIndex(e => e[0].toLowerCase() === currentIcon) + 1;
		const iconElem = document.getElementById(icon);
		const iconElemPosition = iconElem.offsetLeft;

		if (iconIndex <= iconIndexCount) {
			if (techIconsArr[iconIndex]) {
				techStackContainer.scrollLeft = iconElemPosition - 80;
				setIcon(techIconsArr[iconIndex][0]);
			} else {
				techStackContainer.scrollLeft = 0
				setIcon("HTML");
			}
		}

		scrollUpHere.scrollIntoView({ behavior: "smooth" });
	}

	return (
		<div className="technologies section-panel">
			<div className="container">
				<h2 className="section-title" id="techStackTitle">My favorite technologies</h2>
				<p className="nogaps">Click on the icon for fun facts!</p>
				<div className="technologies-list-container" id="techStackContainer">
					{
						Object.keys(techIcons).map((key, index) =>
							<span key={index} className={`tech-icon-container ${key === icon ? "active" : ""}`} id={key} onClick={() => handleClick(key)}>
								{techIcons[key]}
								<span className="icon-label">{key}</span>
							</span>)
					}
				</div>

				<div className={`fun-fact-parent ${icon && "active"}`}>

					{
						icon &&
						<div className="tech-desc">
							<p className="nogaps">{techDesc[icon.toLowerCase()]}</p>
						</div>
					}

					<div className="fun-fact-masonry" ref={gridRef}>
						{
							icon && funFactData[icon.toLowerCase()].map((item, index) =>
								<div key={item.id} className="fun-fact-panel">
									<div className="fun-fact-header">
										<h3 className="fun-fact-title">{item.title}</h3>
										<p className="fun-fact-description">{item.description}</p>
									</div>
									<div className="fun-fact-body" style={{ backgroundImage: `url(${require(`../../assets/images/utilities/stack/${item.image}`)})` }}></div>
								</div>)
						}
						<div className="fun-fact-sizer"></div>
					</div>

					{
						icon &&
						<>
							<small className="color-gray attribution-note">Thanks <a href="https://www.freepik.com/">Freepik</a> for all the vector images.</small>
							<div className="text-center" style={{ marginTop: 20 }}>
								<button className="close-btn" onClick={() => setIcon(false)}><GrClose /></button>
								<span style={{ padding: "0 5px" }}></span>
								<button className="next-btn" onClick={handleNext}><BsChevronRight /></button>
							</div>
						</>
					}

				</div>
			</div>
		</div>
	)
}

export default Technologies;