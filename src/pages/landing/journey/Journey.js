import Timeline from "./Timeline";
import { techJourneyData } from "../../../assets/data/landingData";

const Journey = () => {
	return (
		<section className="Journey section-panel">
			<div className="container">
				<div className="Journey-timeline" id="journeyTimeline">
					<h1 className="section-title text-center">My tech journey</h1>

					<div className="timeline--list">
						{techJourneyData.map((item, key) => <Timeline key={key} id={`timeline-${key}`} timeline={item} />)}
					</div>
				</div>
			</div>
		</section>
	)
}

export default Journey;