import timelineDot from "../../../assets/images/utilities/circle.jpg";

const Timeline = ({ id, timeline }) => {
	return (
		<div className="box-panel" id={id}>
			<div className="timeline--item">
				<img src={timelineDot} className="timelineDot ripple-image" alt="" />
				<div className="timeline--content">
					<p className="timeline--title">{timeline.org}</p>
					<p>{timeline.story}</p>
					{timeline.ref?.name && <p>Visit <a href={timeline.ref.url} className="text-link" target="_new">{timeline.ref.name}</a></p>}
				</div>
			</div>
		</div >
	)
}

export default Timeline;