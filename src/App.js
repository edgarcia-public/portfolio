import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Main from "./pages/Main";
import Landing from "./pages/landing/Landing";

const App = () => {

	const router = createBrowserRouter([
		{
			path: "/",
			element: <Main />,
			children: [
				{
					path: "",
					element: <Landing />
				},

			]
		}
	])

	return (
		<div className="App">
			<RouterProvider router={router} />
		</div>
	);
}

export default App;